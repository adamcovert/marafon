$(document).ready(() => {

  console.log(`document ready`)

  var myWorksSwiper = new Swiper('.s-my-works__slider .swiper-container', {
    slidesPerView: 2,
    spaceBetween: 40,
    centeredSlides: true,
    loop: true,
    autoplay: {
      delay: 5000,
    },
    speed: 500,
    pagination: {
      el: '.s-my-works__slider-progressbar',
      type: 'progressbar',
    },
    breakpoints: {
      320: {
        spaceBetween: 15
      },
      768: {
        spaceBetween: 40
      }
    }
  });

  var scroll = new SmoothScroll('a[href*="#"]');
});